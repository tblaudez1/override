#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main() {
    uint32_t i = 0;
    uint8_t buffer[32];

    fgets(buffer, 32, stdin);
    while (i < strlen(buffer)) {
        if (buffer[i] > 64 && buffer[i] <= 90) {
            buffer[i] ^= 0x20;
        }
        i++;
    }
    printf(buffer);
    exit(0);
}