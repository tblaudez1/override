# Level 06

## Observations

1. The program asks for a username and a serial
2. The program computes a hash from the username and compares it to the given serial, if they match we are allowed in the system.
3. The program exits if it detects that it is executed in a debugger.

## Plan

At some point, the program will compute the correct hash for our username and compare it to our serial.  
If we can get the debugger up to that point, we would be able to read the stack and find the correct hash.

First we need to bypass the anti-debugging protection, it's fairly easy.  
Debugging with *GDB* or *Radare2*, we need to locate the instruction where the program checks the return of `ptrace()` (*which will be `-1` because we are debugging the program*) and change its value to `0` :

```txt
0x080487b5  e836feffff  call sym.imp.ptrace
0x080487ba  83f8ff      cmp eax, 0xffffffff
0x080487bd  752e        jne 0x80487ed
```
The address of the instruction we are looking for is **0x080487BA**, we will put a breakpoint here.  

Next we need to find the instruction where the program is comparing our serial with the one it computed from our username :

```txt
0x08048866  3b45f0  cmp eax, dword [var_10h]
0x08048869  7407    je 0x8048872
```

The address of the instruction we are looking for is **0x08048866**, we will put a breakpoint here.  
We want to be able to read the content of `[var_10h]` (*arbitrary name for a stack variable in Radare2*).  

Let's go for it

### GDB

```txt
(gdb) b *0x080487ba
    Breakpoint 1 at 0x80487ba

(gdb) b *0x08048866
    Breakpoint 2 at 0x8048866

(gdb) run
    Username : username
    Password : 42
    # [...]
    Breakpoint 1, 0x080487ba in auth ()

(gdb) set $eax=0 # Set return value of ptrace() to 0

(gdb) continue
    Breakpoint 2, 0x08048866 in auth ()

(gdb) x/d $ebp-0x10  # Read 16 bytes from top of stack in decimal form
    0xffffd698:     6233783
```

### Radare2

```txt
[0xf7f8b070]> db 0x080487ba # Debug Breakpoint (create a new one)

[0xf7f8b070]> db 0x08048866

[0xf7f8b070]> dc # Debug Continue
    Username : username
    Password : 42
    # [...]
    hit breakpoint at: 0x80487ba

[0x080487ba]> dr eax=0 # Debug Register (set return value of ptrace() to 0)
0xffffffff -> 0x00000000

[0x080487ba]> dc
hit breakpoint at: 0x8048866

[0x08048866]> afvd # Analyze Function Variable Display
arg arg_8h = 0xff80a400 = 4286620716
var var_ch = 0xff80a3ec = 7
var var_10h = 0xff80a3e8 = 6233783 <--- This one's our guy.
var var_14h = 0xff80a3e4 = 7
arg arg_ch = 0xff80a404 = 42
var var_4h = 0xff80a3d8 = 1
var var_sp_ch = 0xff80a3e0 = 4160120192
var var_8h = 0xff80a3dc = 0
```

So now we know that the correct serial for the username `"username"` is **6233783**.  

**Warning**: Value can change between VMs.