# Level 08

## Observations

1. The program takes a file as parameter and copies it to the `backups/` folder.
2. The program opens files `argv[1]` and `./backups/ + argv[1]`

## Plan

Since the program is ran with `level09` privileges, it can open `/home/users/level09/.pass` ; we'll make it copy the file to the `backups/` folder so we can read its content.

Only, if `/home/users/level09/.pass` is given as program argument, it will try to create `./backups/home/users/level09/.pass` which doesn't exist.  
We're also not allowed to create directories in the `~/backups` folder.

However, the program is only looking for the `backups/` folder **in the current directory** and we are allowed to creates as many directories as we want in `/tmp`.  
We can make it so that the path `./backups/home/users/level09/` exists within `/tmp` and call the program on the flag file.  
We'll then have the content of the flag inside `/tmp/backups/home/users/level09/.pass`

## Payload

```bash
mkdir -p /tmp/backups/home/users/level09 ; cd /tmp; ~/level08 /home/users/level09/.pass ; cat backups/home/users/level09/.pass
```