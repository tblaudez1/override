# Level 03

## Observations

1. The input is casted into an integer password with `scanf("%d")`
2. The integer password is substracted to the value `0x1337d00d`
3. If the result of the substraction is between **0** and **21** exclusive, it is sent to the `decrypt()` function that can potentially spawn an elevated shell ; otherwise a random value is sent to `decrypt()`.

## Plan

The decryption algorithm in the decrypt function is a hot mess of senseless operations, no point in trying to understand that.  
We know that our input is sent to the decrypt function only if **0** < `(0x1337d00d - input)` < **21**.  
That means there is only **21** possible passwords from `0x1337cff8` (322424824) to `0x1337d00d` (322424845) ; we can brute-force it.

## Payload

```bash
for i in {322424824..322424845}; do (echo $i; cat -) | ./level03 ; done
```

Keep pressing the Enter key until you are in a shell.