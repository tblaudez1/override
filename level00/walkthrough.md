# Level 00

## Observations

1. Our input is converted to an integer password using `scanf("%d")`.
2. Our integer password is then compared against `5276` and opens a shell if the two values match.

## Plan

Give the program the correct value of `5276`.


```bash
(printf "5276\n" ; cat -) | ./level00
```